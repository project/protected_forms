<?php

/**
 * @file
 * Installation functions for protected_forms module.
 */

use Drupal\user\RoleInterface;

/**
 * Implements hook_install().
 */
function protected_forms_install($is_syncing) {
  if (!$is_syncing) {
    user_role_change_permissions(RoleInterface::AUTHENTICATED_ID, [
      'bypass protected forms validation' => FALSE,
    ]);
  }
}

/**
 * Implements hook_uninstall().
 */
function protected_forms_uninstall() {
  \Drupal::state()->delete('protected_forms.rejected');
}

/**
 * Implements hook_requirements().
 */
function protected_forms_requirements($phase) {
  // Provide stats for rejected submissions on the status page.
  $requirements = [];
  $rejected = \Drupal::state()->get('protected_forms.rejected', 0);
  $requirements['protected_forms'] = [
    'title' => t('Protected forms'),
    'value' => t('Total of @count submissions containing <a href="@patterns">spam patterns</a> have been rejected.',
      [
        '@count' => $rejected,
        '@patterns' => '/admin/config/content/protected_forms',
      ]
    ),
    'severity' => REQUIREMENT_INFO,
  ];
  return $requirements;
}

/**
 * Implements hook_update_N().
 */
function protected_forms_update_8001() {
  if (\Drupal::config('protected_forms.settings')->get('protected_forms.allowed_scripts') === NULL) {
    \Drupal::configFactory()->getEditable('protected_forms.settings')
      ->set('protected_forms.allowed_scripts', ['Latin' => 'Latin'])
      ->save();
  }
  if (\Drupal::config('protected_forms.settings')->get('protected_forms.check_quantity') === NULL) {
    \Drupal::configFactory()->getEditable('protected_forms.settings')
      ->set('protected_forms.check_quantity', '50')
      ->save();
  }
}

/**
 * Swap rejected submissions from configuration item to state api value.
 */
function protected_forms_update_8002() {
  $rejected = \Drupal::config('protected_forms.settings')->get('protected_forms.rejected');

  if ($rejected) {
    \Drupal::state()->set('protected_forms.rejected', $rejected);
    \Drupal::configFactory()->getEditable('protected_forms.settings')
      ->clear('protected_forms.rejected')
      ->save();
  }
}

/**
 * Update the schema definition.
 */
function protected_forms_update_8003() {
  $settings = \Drupal::configFactory()->getEditable('protected_forms.settings');
  // Clear unused "langcode" configuration, if it exists:
  $langcode = $settings->get('protected_forms.langcode');
  if ($langcode) {
    $settings
      ->clear('protected_forms.langcode')
      ->save();
  }
  // Adjust value for "log_rejected" to conform with the schema:
  $settings->set('protected_forms.log_rejected', (bool) $settings->get('protected_forms.log_rejected'))
    ->save();
}

/**
 * Add new setting 'excluded_forms'.
 */
function protected_forms_update_8004() {
  $settings = \Drupal::configFactory()->getEditable('protected_forms.settings');
  $settings->set('excluded_forms', [
    'user_login_form',
    'user_register_form',
    'user_pass',
  ])->save();
}

/**
 * Fix structure of new 'excluded_forms' settings.
 */
function protected_forms_update_8005() {
  // protected_forms_update_8004 introduced a wrong structure for
  // excluded_forms parent.
  $settings = \Drupal::configFactory()->getEditable('protected_forms.settings');
  $settings->set('protected_forms.excluded_forms', [
    'user_login_form',
    'user_register_form',
    'user_pass',
  ])->clear('excluded_forms')
    ->save();
}

/**
 * Remove potentially unwanted reject patterns.
 */
function protected_forms_update_8006() {
  $reject_patterns = \Drupal::config('protected_forms.settings')->get('protected_forms.reject_patterns');
  $unwanted_patterns = [
    'http://',
    'https://',
    'www',
    'href=',
    'great website',
    'keyword',
    '    :',
    '   :',
    'look at the',
  ];
  $reject_patterns = str_ireplace($unwanted_patterns, '', $reject_patterns);
  $reject_patterns_array = array_filter(explode(',', $reject_patterns), "trim");
  $reject_patterns = implode(",", $reject_patterns_array);
  \Drupal::configFactory()->getEditable('protected_forms.settings')
    ->set('protected_forms.reject_patterns', $reject_patterns)
    ->save();
}

/**
 * Change the structure of the 'reject_patterns' and 'allowed_patterns' setting.
 */
function protected_forms_update_8007() {
  // Turn the settings from comma-separated strings into an array.
  $config = \Drupal::config('protected_forms.settings');
  $rejectPatternsString = $config->get('protected_forms.reject_patterns');
  $allowedPatternsString = $config->get('protected_forms.allowed_patterns');
  $rejectPatternsArray = preg_split("/(\n|,)/", $rejectPatternsString);
  if (!empty($rejectPatternsArray)) {
    foreach ($rejectPatternsArray as &$reject_pattern) {
      trim($reject_pattern, "\r");
    }
  }
  $allowedPatternsArray = preg_split("/(\n|,)/", $allowedPatternsString);
  if (!empty($allowedPatternsArray)) {
    foreach ($allowedPatternsArray as &$allowed_pattern) {
      trim($allowed_pattern, "\r");
    }
  }
  \Drupal::configFactory()
    ->getEditable('protected_forms.settings')
    ->set('protected_forms.reject_patterns', $rejectPatternsArray)
    ->set('protected_forms.allowed_patterns', $allowedPatternsArray)
    ->save();
}
