INTRODUCTION
------------
**Protected Forms** is a light-weight, non-intrusive spam protection
module that enables rejection of node, comment, webform, user profile,
contact form, private message and revision log submissions which contain
undesired language characters or preset patterns.

HOW IT WORKS
------------
If a user attempts to add a content with a trigger pattern in the name,
subject, body or any other _textarea_  or _textfield_ type field, then
the submission is rejected giving the preset error message.

Admins can configure any role's permission to 
**Bypass Protected Forms validation**.

The stats for rejected submissions can be tracked on the **Status Report** page.

The rejected submissions can be viewed on the **Recent log messages** page.

REQUIREMENTS
------------
No special requirements.

INSTALLATION
------------
Download and place the recommended version of the module in your website's
modules directory, go to the **Extend** page (`/admin/modules`) and enable the
**Protected Forms** module.

Alternatively, just run on CLI:
```
composer require drupal/protected_forms
drush -y en protected_forms
```

CONFIGURATION
-------------
Go to the **Protected Forms** configuration page
(`/admin/config/content/protected_forms`) and set the reject message
text and the trigger patterns for rejection. Set needed permissions on
`/admin/people/permissions#module-protected_forms`.

TROUBLESHOOTING
---------------
Report all issues on
https://www.drupal.org/project/issues/search/protected_forms.
