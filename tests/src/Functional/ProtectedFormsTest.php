<?php

namespace Drupal\Tests\protected_forms\Functional;

use Drupal\comment\Tests\CommentTestTrait;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\user\RoleInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * {@inheritdoc}
 */
class ProtectedFormsTest extends BrowserTestBase {
  use NodeCreationTrait, CommentTestTrait;
  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';
  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'comment',
    'protected_forms',
  ];

  /**
   * {@inheritdoc}
   */
  public function testNodeCommentRejectedSubmissionPrintsCorrectMessage() {
    // Create an article content type (if it doesn't exist already).
    $types = NodeType::loadMultiple();
    if (empty($types['article'])) {
      $this->drupalCreateContentType([
        'type' => 'article',
        'name' => 'Article',
      ]);
    }

    // Create comment field on article.
    $this->addDefaultCommentField('node', 'article');

    // Create an article node.
    $this->drupalCreateNode([
      'title' => 'An Article',
      'type' => 'article',
    ])->save();

    // Allow anonymous users to post comments.
    $permissions = [
      'access content',
      'access comments',
      'post comments',
      'skip comment approval',
    ];
    user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, $permissions);

    // Add a custom string to the reject patterns list.
    $rejectPatterns = $this->config('protected_forms.settings')->get('protected_forms.reject_patterns');
    $inputString = ' monkey';
    $rejectPatterns[] = $inputString;
    $this->config('protected_forms.settings')
      ->set('protected_forms.reject_patterns', $rejectPatterns)
      ->save();

    // Attempt to post a spam comment using the newly added reject pattern.
    $input = [
      'edit-comment-body-0-value' => $inputString,
    ];
    $this->drupalGet('node/1');
    $this->submitForm($input, 'Save');

    // Ensure we get a 200 response.
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);

    // Ensure the rejected message is displayed.
    $message = $this->config('protected_forms.settings')->get('protected_forms.reject_message');
    $this->assertSession()->responseContains($message);
    $this->assertSession()->responseNotContains('Your comment has been posted.');

    // Attempt to post a normal comment.
    $input = [
      'edit-comment-body-0-value' => 'a normal comment',
    ];
    $this->drupalGet('node/1');
    $this->submitForm($input, 'Save');

    // Ensure we get a 200 response.
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);

    // Ensure our normal post was posted.
    $this->assertSession()->responseContains('Your comment has been posted.');
    $this->assertSession()->responseContains('a normal comment');

    // Attempt to post a comment that contains a phrase blocked by default.
    $input = [
      'edit-comment-body-0-value' => 'You can win something here!',
    ];
    $this->drupalGet('node/1');
    $this->submitForm($input, 'Save');

    // Ensure we get a 200 response.
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);

    // Ensure the rejected message is displayed.
    $message = $this->config('protected_forms.settings')->get('protected_forms.reject_message');
    $this->assertSession()->responseContains($message);
    $this->assertSession()->responseNotContains('Your comment has been posted.');

    // Attempt to post a comment that contains multiple blocked phrases.
    $input = [
      'edit-comment-body-0-value' => 'You can win this for free or buy it!',
    ];
    $this->drupalGet('node/1');
    $this->submitForm($input, 'Save');

    // Ensure we get a 200 response.
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);

    // Ensure the rejected message is displayed.
    $message = $this->config('protected_forms.settings')->get('protected_forms.reject_message');
    $this->assertSession()->responseContains($message);
    $this->assertSession()->responseNotContains('Your comment has been posted.');

    // Attempt to post a comment with only a partial match that should be valid
    // because only whole patterns are matched.
    // (Whole pattern: "are whitelisted")
    $input = [
      'edit-comment-body-0-value' => 'I am whitelisted.',
    ];
    $this->drupalGet('node/1');
    $this->submitForm($input, 'Save');

    // Ensure we get a 200 response.
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);

    // Ensure our normal post was posted.
    $this->assertSession()->responseContains('Your comment has been posted.');
    $this->assertSession()->responseContains('I am whitelisted.');
  }

  /**
   * Tests that excluded forms are not protected.
   */
  public function testExcludedFormsNotProtected() {
    // Enter spam on the user registration form, which is excluded by default.
    $input = [
      'mail' => 'admin@admin123.com',
      'name' => 'free win',
    ];
    $this->drupalGet('/user/register');
    $this->submitForm($input, 'Create new account');
    $this->assertSession()->responseContains('A welcome message with further instructions has been sent to your email address.');
    $this->assertSession()->responseNotContains('Oops! It looks like your post contains spam content.');
    // Remove the account creation from from the excluded forms and try again.
    \Drupal::configFactory()->getEditable('protected_forms.settings')
      ->set('protected_forms.excluded_forms', [])
      ->save();
    $this->drupalGet('/user/register');
    $this->submitForm($input, 'Create new account');
    $this->assertSession()->responseNotContains('A welcome message with further instructions has been sent to your email address.');
    $this->assertSession()->responseContains('Oops! It looks like your post contains spam content.');
  }

}
